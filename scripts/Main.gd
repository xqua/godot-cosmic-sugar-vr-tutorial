extends Spatial

func _ready():
  # This will try and find a compatible and connected VR interface
	var VR = ARVRServer.find_interface("OpenVR")

	# Then we test, to see if we found one, and if it initialized ok, because if we did not, well we need to write a catch or someting.
	if VR and VR.initialize():
		# This tells Godot that we have a VR headset, so please activate all the magic
		get_viewport().arvr = true

		# This is the necessary trick so that we don't need to deactivate the HDR capacity of Godot
		get_viewport().keep_3d_linear = true

		# Also, the physics FPS in the project settings is also 90 FPS. This makes the physics
		# run at the same frame rate as the display, which makes things look smoother in VR!
		Engine.target_fps = 90
	else:
		# Here you will want to write some function that says that no VR headset was found -> sad
		pass
