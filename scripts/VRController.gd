extends ARVRController

onready var particles = get_node("../../Particles")
onready var particles_material = particles.get_process_material()

var shader_controller_position
var shader_controller_state
var trigger_pressed = false
var grip_pressed = false

func _ready():
	if controller_id == 1:
		shader_controller_position = "LeftController_position"
		shader_controller_state = "LeftController_state"
	elif controller_id == 2:
		shader_controller_position = "RightController_position"
		shader_controller_state = "RightController_state"
	else:
		print("The controller ID is outside of the legal values (1: Left, 2: Right)")
	connect("button_pressed", self, "_event_button_pressed")
	connect("button_release", self, "_event_button_released")

func _physics_process(delta):
	_set_position()
	_set_state()

func _set_position():
	var position = transform.origin
	particles_material.set_shader_param(shader_controller_position, position)

func _set_state():
	if trigger_pressed and not grip_pressed:
		particles_material.set_shader_param(shader_controller_state, 1)
	elif grip_pressed and not trigger_pressed:
		particles_material.set_shader_param(shader_controller_state, -1)
	elif grip_pressed and trigger_pressed:
		particles_material.set_shader_param(shader_controller_state, 0)
	else:
		particles_material.set_shader_param(shader_controller_state, 0)

func _event_button_pressed(button_index):
	match button_index:
		15:
			print("Trigger pressed")
			trigger_pressed = true
		2:
			print("Grip pressed")
			grip_pressed = true

func _event_button_released(button_index):
	match button_index:
		15:
			print("Trigger released")
			trigger_pressed = false
		2:
			print("Grip released")
			grip_pressed = false
