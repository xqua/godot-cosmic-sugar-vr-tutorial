# Godot VR Cosmic Sugar Like Tutorial

I wanted to learn some more GPU computing, and particle shaders seemed like a fun thing to learn about. So I decided to try and recreate a game in Godot that mimics Cosmic Sugar, and old VR title that blew my mind back in the days. From this I created a tutorial so that others can do the same.

You can find the tutorial on the main [Godot Website](), as well as on my [own website](http://leo.blondel.ninja/CosmicSugarGodot/)