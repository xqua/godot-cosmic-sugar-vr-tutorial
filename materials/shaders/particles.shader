shader_type particles; //Well now you know how to fix that error above right? 

// The controlers positions as a vec3 for X Y and Z
uniform vec3 LeftController_position = vec3(5.0);
uniform vec3 RightController_position = vec3(-5.0);

// The controller states so that we can switch between attracting and pushing away
uniform int LeftController_state = 1;
uniform int RightController_state = 1;

// This creates a random number from a seed number
float rand_from_seed(in uint seed) {
  int k;
  int s = int(seed);
  if (s == 0)
    s = 305420679;
  k = s / 127773;
  s = 16807 * (s - k * 127773) - 2836 * k;
  if (s < 0)
    s += 2147483647;
  seed = uint(s);
  return float(seed % uint(65536)) / 65535.0;
}

uint hash(uint x) {
  x = ((x >> uint(16)) ^ x) * uint(73244475);
  x = ((x >> uint(16)) ^ x) * uint(73244475);
  x = (x >> uint(16)) ^ x;
  return x;
}

vec3 field_gravity(vec3 position, vec3 center, float scale) {
  return normalize(-position + center) * scale;
}

vec3 field_periodic(vec3 position) {
	float A = 1.0;
	float B = sqrt(2.0 / 3.0);
	float C = sqrt(1.0 / 3.0);
    vec3 velocity = vec3(A * sin(position.z) + C * cos(position.y), 
						 B * sin(position.x) + A * cos(position.z), 
						 C * sin(position.y) + B * cos(position.x));
	return velocity;
}

vec3 get_velocity(vec3 position) {
  // we divide by the number of controller such that the maximum velocity is bounded to our value
  float scale = 10.0 / 2.0;
  // We compute the vector field with the two centers
  vec3 left_velocity = field_gravity(position, LeftController_position, scale) * float(LeftController_state);
  vec3 right_velocity = field_gravity(position, RightController_position, scale) * float(RightController_state);
  // Finally we sum the two vector fields and return the velocity
  vec3 velocity = left_velocity + right_velocity;
//  velocity = field_periodic(position);
  return velocity;
}

void vertex() {
	if (RESTART) {
		uint seed_x = hash(NUMBER + uint(27) + RANDOM_SEED);
		uint seed_y = hash(NUMBER + uint(43) + RANDOM_SEED);
		uint seed_z = hash(NUMBER + uint(111) + RANDOM_SEED);
		vec3 position = vec3(rand_from_seed(seed_x) * 2.0 - 1.0,
		                     rand_from_seed(seed_y) * 2.0 - 1.0,
		                     rand_from_seed(seed_z) * 2.0 - 1.0);
		TRANSFORM[3].xyz = position * 20.0;
		COLOR = vec4(pow(position, vec3(2.0)), 1.0);
	}
	VELOCITY = get_velocity(TRANSFORM[3].xyz);
}
	